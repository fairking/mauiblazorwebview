﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System.Diagnostics;

namespace MauiApp1;

public partial class MainPage : ContentPage
{
	public MainPage()
	{
		InitializeComponent();
	}

    private async void button_Clicked(object sender, EventArgs e)
    {
		var js = webView.Handler.MauiContext.Services.GetService<IJSRuntime>();
		var result = await js.InvokeAsync<string>("executeMe", "DEF");
		Debug.WriteLine(result);
	}
}
