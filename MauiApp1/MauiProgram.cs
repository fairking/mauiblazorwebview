﻿using Microsoft.AspNetCore.Components.WebView.Maui;
using MauiApp1.Data;
using Microsoft.JSInterop;

namespace MauiApp1;

public static class MauiProgram
{
	public static IServiceProvider Services { get; set; }

	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
			});

		builder.Services.AddMauiBlazorWebView();

		builder.Services.AddBlazorWebViewDeveloperTools();

		builder.Services.AddSingleton<WeatherForecastService>();

		var app = builder.Build();

		Services = app.Services;

		return app;
	}

	[JSInvokable]
	public static async Task<string> CallMeFromJs(string a)
	{
		return a + " OK!";
    }
}
